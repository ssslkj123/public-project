# K8s WebTerminal 开发实战 训练营(3天)

## 项目初始化

```sh
# 初始化项目
$ mdkir web_terminal
$ cd web_terminal
$ go mod init gitlab.com/go-course-project/public-project/web_terminal
# 使用 vscode 打开该工程
$ code .
```

## 目录结构说明

+ main.go： 项目入口文件, WebSocket Server 就在这里实现
+ ui: 前端页面目录
+ terminal: WebSocket Terminal 具体的业务实现
+ k8s: k8s 相关功能封装与验证

## k8s 包封装

封装并对业务功能进行验证


## 数据与指令

+ BinaryMessage: 数据

+ TextMessage: 指令


## UI

```sh
cd ui
npm install @xterm/xterm
code .
```

## 打包

